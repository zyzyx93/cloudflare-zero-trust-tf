# Cloudflare Zero Trust

## Filtering through contents categories

Cloudflare Gateway allows you to block known and potential security risks on the public Internet, as well as specific categories of content. Domains are categorized by Cloudflare Radar.

You can block security and content categories by creating DNS or HTTP policies. Once you have configured your policies, you will be able to inspect network activity and the associated categories in your Gateway logs.

[Domain categories](https://developers.cloudflare.com/cloudflare-one/policies/filtering/domain-categories/)

1. As part of Company Content Restriction policy, block web content
   that contains any adult material, violence, child abuse and gambling.
   | Category ID | Category Name | Subcategory ID | Subcategory Name
   | --- | ----------- |---- |----|
   | 2 | Adult Themes | 67 | Adult Themes|
   | 2 | Adult Themes | 125 | Nudity|
   | 2 | Adult Themes | 133 | Pornography|
   | 29 | Adult Themes | 165 | Violence|
   | 29 | Adult Themes | 166 | Weapons|
   | 31 | Blocked | 170 | Child Abuse|
   | 8 | Gambling | 99 | Gambling|
2. Block any requests to potential websites hosting malicious content.
   | Category ID | Category Name | Subcategory ID | Subcategory Name
   | --- | ----------- |---- |----|
   | 21 | Security threats | 117 | Malware|
3. Block any requests made to potential C&C or Botnet sites
   | Category ID | Category Name | Subcategory ID | Subcategory Name
   | --- | ----------- |---- |----|
   | 21 | Security threats | 80 | Command and Control & Botnet|
4. Block any requests to suspicious sites that are suspected of cryptomining, part of phishing campaign, spam related, spyware related, typosquatting and DGA sites.
   | Category ID | Category Name | Subcategory ID | Subcategory Name
   | --- | ----------- |---- |----|
   | 21 | Security threats | 83 | Cryptomining|
   | 21 | Security threats | 131 | Phishing|
   | 21 | Security threats | 151 | Spam|
   | 21 | Security threats | 153 | Spyware|
   | 21 | Security threats | 178 | Typosquatting & Impersonation|
   | 21 | Security threats | 176 | Domain Generation Algorithm|
5. There is a recent list of suspected sites with bad reputation that needs to be blocked. Respond to this by blocking any HTTP requests to the following sites:

   - a. 185-220-102-244.torservers.net
   - b. 185-220-102-246.torservers.net
   - c. 185-220-102-247.torservers.net
   - d. 185-220-102-243.torservers.net

   Please ensure you use the most minimum entries possible to solve this where possible.Ensure you do the necessary to start effectively enforcing the policy on your user device.

6. Implement a policy that prevents potential abuse of sharing private content to external by blocking access to the following all supported SaaS file sharing sites except Microsoft OneDrive
   | ID | Application Name|
   | --- | ----------------|
   | 767 | 1fichier |
   | 769 | Bajoo |
   | 554 | Google Drive |
   | 1174 | Google Play Store |
   | 521 | box|
   | 531 | Cyberduck|
   | 538 | Dropbox |
   | 571 | iCloud |
   | 637 | WeTransfer |

7. Implement a policy configuration using your Terraform config to
   deploy a policy that blocks the following destination IPs:

   - a) 45.83.65.129 (Germany)
   - b) 167.71.67.189 (Netherlands)

8. There will be a growing list of websites that will be
   blocked and you do not want to repeat the resources each time you
   define a new site to block. So just edit these 2 files with and IP or hostname
   that you want to block

   - IP use ./sus-ips.txt

   ```txt
   45.83.65.129
   167.71.67.189
   ```

   - hostname use ./sus-domains.txt

   ```txt
   185-220-102-244.torservers.net
   185-220-102-246.torservers.net
   185-220-102-247.torservers.net
   185-220-102-243.torservers.net
   ```

9. Blocks the following social networking applications (Please refer to gateway-api.json for App ID)

   | ID  | Application Name |
   | --- | ---------------- |
   | 572 | Instagram        |
   | 628 | TikTok           |
   | 618 | Snapchat         |

## Useful API Endpoint

### List Application and Application Type mappings

[App Type Mapping](https://developers.cloudflare.com/api/operations/zero-trust-gateway-application-and-application-type-mappings-list-application-and-application-type-mappings)

```bash
curl --request GET \
 --url https://api.cloudflare.com/client/v4/accounts/identifier/gateway/app_types \
 --header 'Content-Type: application/json' \
 --header 'X-Auth-Email: '
```

### List Zero Trust Gateway Rules

[Gateway Rules](https://developers.cloudflare.com/api/operations/zero-trust-gateway-rules-list-zero-trust-gateway-rules)

```bash
curl --request GET \
  --url https://api.cloudflare.com/client/v4/accounts/identifier/gateway/rules \
  --header 'Content-Type: application/json' \
  --header 'X-Auth-Email: '
```

## Results

### Policy Being Updated from Gitlab Pipeline

<img src="./images/dns-policy-result.png" style="height: 600px; width:1400px;"/>

### Sites that being blocked based on content category

<img src="./images/site-blocked.png" style="height: 600px; width:1400px;"/>

### Known SaaS File Sharing App being block except for MS One Drive

<img src="./images/application-blocked.png" style="height: 600px; width:1400px;"/>

### Social media app and site being blocked based on Cloudfalre Radar

<img src="./images/social-media-blocked.png" style="height: 600px; width:1400px;"/>

## References:

[Secure Web Gateway - DNS policies](https://developers.cloudflare.com/cloudflare-one/policies/filtering/dns-policies/)

## Improvement

1. Utilize teams list as a blocking source.
2. [Enable API/Terraform Read Only Mode](https://developers.cloudflare.com/cloudflare-one/api-terraform/#:~:text=To%20enable%20read%2Donly%20mode,%2FTerraform%20read%2Donly%20mode.)
