variable "cloudflare_api_token" {
  type        = string
  description = "cloudflare API token"
}

variable "cloudflare_account_id" {
  type        = string
  description = "cloudflare account ID"
}
