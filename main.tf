resource "cloudflare_teams_rule" "block_illicit_content" {
  account_id  = var.cloudflare_account_id
  name        = "Block Illicit Content"
  description = "As part of Company Content Restriction policy, block web content that contains any adult material, violence, child abuse and gambling."
  enabled     = true
  precedence  = 10000
  # Block all security risks
  filters = ["dns"]
  traffic = "any(dns.security_category[*] in {67 125 133 165 166 170 99})"
  action  = "block"

  rule_settings {
    block_page_enabled = true
  }
}

resource "cloudflare_teams_rule" "block_malware" {
  account_id  = var.cloudflare_account_id
  name        = "Block Malware"
  description = "Block any requests to potential websites hosting malicious content."
  enabled     = true
  precedence  = 10000
  # Block all security risks
  filters = ["dns"]
  traffic = "any(dns.security_category[*] in {177})"
  action  = "block"

  rule_settings {
    block_page_enabled = true
  }
}

resource "cloudflare_teams_rule" "block_cc" {
  account_id  = var.cloudflare_account_id
  name        = "Block C&C"
  description = "Block any requests made to potential C&C or Botnet sites"
  enabled     = true
  precedence  = 10000
  # Block all security risks
  filters = ["dns"]
  traffic = "any(dns.security_category[*] in {80})"
  action  = "block"

  rule_settings {
    block_page_enabled = true
  }
}

resource "cloudflare_teams_rule" "block_crypto_spy" {
  account_id  = var.cloudflare_account_id
  name        = "Block Crypto Spy"
  description = "Block any requests to suspicious sites that are suspected of cryptomining, part of phishing campaign, spam related, spyware related, typosquatting and DGA sites."
  enabled     = true
  precedence  = 10000
  # Block all security risks
  filters = ["dns"]
  traffic = "any(dns.security_category[*] in {83 131 151 153 178 176})"
  action  = "block"

  rule_settings {
    block_page_enabled = true
  }
}

locals {
  sus_domains = [for line in split("\n", file("sus-domains.txt")) : chomp(line)]
  sus_domains_format = [
    for domain in local.sus_domains : format("\"%s\"", domain)
  ]
  sus_domain_join = join(" ", local.sus_domains_format)
}

# Blocking Bad Site
resource "cloudflare_teams_rule" "block_bad_site" {
  account_id  = var.cloudflare_account_id
  name        = "Block Bad Site"
  description = "List of suspected sites with bad reputation"
  enabled     = true
  precedence  = 10000
  action      = "block"
  filters     = ["http"]
  traffic     = "http.request.host in {${local.sus_domain_join}}"
  rule_settings {
    block_page_enabled = true
  }
}

# Block file sharing Service Except MS One-Drive
resource "cloudflare_teams_rule" "block_file_sharing" {
  account_id  = var.cloudflare_account_id
  name        = "Block Non-OneDrive"
  description = "Blocks access to all supported SaaS file sharing sites except Microsoft OneDrive"
  enabled     = true
  precedence  = 10000
  action      = "block"
  filters     = ["l4"]
  traffic     = "any(app.ids[*] in {767 769 554 1174 521 531 538 571 637})"
  rule_settings {
    block_page_enabled = true
  }
}

locals {
  sus_ips      = [for line in split("\n", file("sus-ips.txt")) : chomp(line)]
  sus_ips_join = join(" ", local.sus_ips)
  sus_ips_trim = trim(local.sus_ips_join, "\"")
}


# Block IP and their Destination Country
resource "cloudflare_teams_rule" "block_ips" {
  account_id = var.cloudflare_account_id

  name        = "Block IPs"
  description = "Block bad IPs"

  enabled    = true
  precedence = 10000

  # Block domain belonging to lists (defined below)
  filters = ["l4"]
  action  = "block"
  traffic = "net.dst.ip in {${local.sus_ips_trim}} and net.dst.geo.country in {\"DE\" \"NL\"}"
  rule_settings {
    block_page_enabled = true
  }

}

# Block social media apps
resource "cloudflare_teams_rule" "block_social_media" {
  account_id  = var.cloudflare_account_id
  name        = "Block Social Media"
  description = "Blocks social networking applications"
  enabled     = true
  precedence  = 10000
  action      = "block"
  filters     = ["l4"]
  traffic     = "any(app.ids[*] in {572 628 618})"
  rule_settings {
    block_page_enabled = true
  }
}

